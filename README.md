# README

Code to test C++11 custom deleter feature in both Windows and Linux.

## Requirements

This code requires the following:

- C++11-compliant compiler in Linux.

- C++11-compliant cross-compiler in Linux if you want to create Windows executable. 
  In this case, Windows port of the compiler's stdc++ library.

## How to build

- Invoke the ```build.sh``` script to build the executable for Linux

- Invoke the ```cross_build.sh``` script to build the executable for Windows in Linux. 
  I'm using Arch Linux mingw-w64 for this in my system. 

