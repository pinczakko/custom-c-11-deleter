#include <iostream>
#include <vector>
#include <memory>
#include <cstdio>
#include <stdexcept>
#include <cerrno>
#include <cstring>

using namespace std;

namespace DeleterTest {

int closeFile(FILE* f) {
    cout << "Calling fclose()" << endl;
    return fclose(f);
}

string FormatErrorMessage(int errNo, const string& msg)
{
    static const int BUF_LEN = 1024;
    vector<char> buf(BUF_LEN);
    strncpy(buf.data(), strerror(errNo), BUF_LEN - 1);

    return string(buf.data()) + "   (" + msg + ")   ";
}

class FileHandlerException : public runtime_error
{
public:
    explicit FileHandlerException(int errNo, const string& msg):
        runtime_error(FormatErrorMessage(errNo, msg)), mErrorNumber(errNo) {}

    int GetErrorNo() const {
        return mErrorNumber;
    }

private:
    int mErrorNumber;
};

class FileHandler {
public:
    explicit FileHandler (const char* path, const char* mode)
try:
        mPath{path}, mMode {mode}
    {
        cout << "FileHandler constructor" << endl;

        FILE* f = fopen(path, mode);

        if (f != NULL) {
            unique_ptr<FILE, int (*)(FILE*)> file{f, closeFile};
            mFile = std::move(file);

        } else {
            throw FileHandlerException(errno, "Failed to open " + string(path));
        }

    } catch (FileHandlerException& e) {

        throw e;
    }

    FileHandler (const FileHandler& rhs) = delete;
    FileHandler& operator= (FileHandler& rhs) = delete;

    FileHandler (FileHandler&& rhs) {
        mPath = rhs.mPath;
        mMode = rhs.mMode;
        mFile = move(rhs.mFile);
    }

    ~FileHandler() {
        cout << "FileHandler destructor" << endl;
    }

    string getPath() {
        return mPath;
    }

    string getMode() {
        return mMode;
    }

private:
    string mPath;
    string mMode;

    unique_ptr<FILE, int (*)(FILE*)> mFile{nullptr, closeFile};
};

} // DeleterTest

int main(int argc, char *argv[]) {

    if (argc != 2) {
        cout << "Usage: " << endl;
        cout << argv[0] << " filename" << endl;
        return 0;
    }

    try {
        DeleterTest::FileHandler f(argv[1], "r");
        cout << "File opened:" << endl;

        cout << "Path: " << f.getPath()  << endl;

        cout << "Mode: " << f.getMode()  << endl;

    } catch (DeleterTest::FileHandlerException& e) {

        cout << "Error!!!" << endl;
        cout << e.what() << endl;
        cout << "errno: " << e.GetErrorNo() << endl;
    }

    cout << "Preparing to exit from " << __func__ << "()\n" ;

    return 0;
}

