#!/bin/bash

#clang++ -g3 -std=c++11 -stdlib=libc++ test.cc -lc++abi -o test
#g++ -O0 -g3 -std=c++11 test.cc -o test

BUILD_DIR=build-native

rm -rvf ${BUILD_DIR} 
mkdir ${BUILD_DIR}
cd ${BUILD_DIR}
cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
make VERBOSE=1 
cd - 

