#!/bin/bash

find . -type f -name "*.cc" | xargs astyle
find . -type f -name "*.cxx" | xargs astyle
find . -type f -name "*.hpp" | xargs astyle

find . -type f -name "*.orig" | xargs rm -vf 
